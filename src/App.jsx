import React, { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";

import { About, Contact, Experience, Feedbacks, Hero, Navbar, Tech, Works, StarsCanvas } from "./components";
import mixpanel from 'mixpanel-browser';
const App = () => {
  useEffect(() => {
    mixpanel.init("324e47e5839f09b5dba273742bfb82e4", { debug: true, track_pageview: true, persistence: 'localStorage' });
    mixpanel.identify('USER_ID');
    mixpanel.track('accuiel');
  }, []);
  return (
    <BrowserRouter>
      <div className='relative z-0 bg-primary'>
        <div className='bg-hero-pattern bg-cover bg-no-repeat bg-center'>
          <Navbar />
          <Hero />
        </div>
        <About />
        <Experience />
        <Tech />
        <Works />
        <Feedbacks />
        <div className='relative z-0'>
          <Contact />
          <StarsCanvas />
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
