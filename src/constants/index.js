import {
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  volkeno,
  // starbucks,
  bakeli,
  tesla,
  // shopify,
  carrent,
  jobit,
  tripguide,
  threejs,
  sendeal
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "Apropos",
  },
  {
    id: "work",
    title: "Projet",
  },
  {
    id: "contact",
    title: "Contact",
  },
  {
    id: "cv",
    title: "mon cv",
  },
];

const services = [
  {
    title: "Developpeur Web ",
    icon: web,
  },
  {
    title: "Developpeur React Native ",
    icon: mobile,
  },
  {
    title: " Developpeur Backend",
    icon: backend,
  },
  {
    title: "Integrateur d'interface utilisateur",
    icon: creator,
  },
];

const technologies = [
  {
    name: "HTML 5",
    icon: html,
  },
  {
    name: "CSS 3",
    icon: css,
  },
  {
    name: "JavaScript",
    icon: javascript,
  },
  {
    name: "TypeScript",
    icon: typescript,
  },
  {
    name: "React JS",
    icon: reactjs,
  },
  {
    name: "Redux Toolkit",
    icon: redux,
  },
  {
    name: "Tailwind CSS",
    icon: tailwind,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
  {
    name: "Three JS",
    icon: threejs,
  },
  {
    name: "git",
    icon: git,
  },
  {
    name: "figma",
    icon: figma,
  },
  {
    name: "docker",
    icon: docker,
  },
];

const experiences = [
  {
    title: "Developpeur web/Mobile et Coach formateur en programmation web",
    company_name: "Volkeno Group (Bakeli School Of Technology)",
    icon: bakeli,
    iconBg: "#383E56",
    date: "Fevrier 2024 _ Present",
    points: [
      "Développer et maintenir des applications Web à l'aide de React.js et d'autres technologies connexes.",
      "Collaborer avec des équipes interfonctionnelles, notamment des concepteurs, des chefs de produits et d'autres développeurs, pour créer des produits de haute qualité.",
      "Mettre en œuvre une conception réactive et assurer la compatibilité entre navigateurs.",
      "Participer aux révisions de code et fournir des commentaires constructifs aux autres développeurs.",
      "Former des etudiants dans le metier du developpement web.",
      "Manager des stagiaire dans la creation d'application web.",
    ],
  },
  {
    title: "Developpeur mobile et coach assistant en programmation",
    company_name: "Volkeno Group (Bakeli School Of Technology)",
    icon: bakeli,
    iconBg: "#E6DEDD",
    date: "fevrier 2023 - Fevrier 2024",
    points: [
      "Former des etudiants dans le metier du developpement web.",
      "Développer et maintenir des applications Web à l'aide de React.js et d'autres technologies connexes.",
      "Collaborer avec des équipes interfonctionnelles, notamment des concepteurs, des chefs de produits et d'autres développeurs, pour créer des produits de haute qualité.",
      "Mettre en œuvre une conception réactive et assurer la compatibilité entre navigateurs.",
      "Participer aux révisions de code et fournir des commentaires constructifs aux autres développeurs.",
    ],
  },
  {
    title: "Developpeur web",
    company_name: "Sendeal.SN",
    icon: sendeal,
    iconBg: "#383E56",
    date: "août 2022 - déc. 2022 · 5 mois",
    points: [
      "Web master.",
      "Concevoir le site de vente en ligne sendeal.sn.",
      "Maintenir le site.",
     
    ],
  },
  {
    title: "Integrateur web",
    company_name: "volkeno",
    icon: volkeno,
    iconBg: "#E6DEDD",
    date: "mars 2022 - aout 2022",
    points: [
      "Developing and maintaining web applications using React.js and other related technologies.",
      "Collaborating with cross-functional teams including designers, product managers, and other developers to create high-quality products.",
      "Implementing responsive design and ensuring cross-browser compatibility.",
      "Participating in code reviews and providing constructive feedback to other developers.",
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "Intégrer la Traduction dans Votre Application ReactJS avec React-i18next",
    name: "Daouda Thiam",
    designation: "React-i18next",
    // company: "Bakeli",
    // image: "../assets/dave.png",
  },
  {
    testimonial:
      "Application PWA avec Reactjs et notification push.",
    name: "Daouda Thiam",
    designation: "Pwa react",
    // company: "Bakeli",
    // image: "https://randomuser.me/api/portraits/men/5.jpg",
  },
  // {
  //   testimonial:
  //     "After Rick optimized our website, our traffic increased by 50%. We can't thank them enough!",
  //   name: "Lisa Wang",
  //   designation: "CTO",
  //   company: "456 Enterprises",
  //   image: "https://randomuser.me/api/portraits/women/6.jpg",
  // },
];

const projects = [
  {
    name: "Car Rent",
    description:
      "Web-based platform that allows users to search, book, and manage car rentals from various providers, providing a convenient and efficient solution for transportation needs.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "mongodb",
        color: "green-text-gradient",
      },
      {
        name: "tailwind",
        color: "pink-text-gradient",
      },
    ],
    image: carrent,
    source_code_link: "https://github.com/",
  },
  {
    name: "Job IT",
    description:
      "Web application that enables users to search for job openings, view estimated salary ranges for positions, and locate available jobs based on their current location.",
    tags: [
      {
        name: "react",
        color: "blue-text-gradient",
      },
      {
        name: "restapi",
        color: "green-text-gradient",
      },
      {
        name: "scss",
        color: "pink-text-gradient",
      },
    ],
    image: jobit,
    source_code_link: "https://github.com/",
  },
  {
    name: "Trip Guide",
    description:
      "A comprehensive travel booking platform that allows users to book flights, hotels, and rental cars, and offers curated recommendations for popular destinations.",
    tags: [
      {
        name: "nextjs",
        color: "blue-text-gradient",
      },
      {
        name: "supabase",
        color: "green-text-gradient",
      },
      {
        name: "css",
        color: "pink-text-gradient",
      },
    ],
    image: tripguide,
    source_code_link: "https://github.com/",
   
  },
];

export { services, technologies, experiences, testimonials, projects };
